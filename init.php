<?php
class Hotkeys_Ru extends Plugin {

	function about() {
		return array(1.0,
			"Mirrors English hotkey map into Russian keyboard layout",
			"fox");
	}

	function init($host) {
		$host->add_hook($host::HOOK_HOTKEY_MAP, $this);
	}

	/**
	 * @param string $str
	 * @param string|array<string> $from
	 * @param string $to
	 * @return string
	 */
	private function mb_strtr(string $str, $from, string $to) : string {
		$keys = [];
		$values = [];

		if (!is_array($from)) {
			preg_match_all('/./u', $from, $keys);
			preg_match_all('/./u', $to, $values);
			$mapping = array_combine($keys[0], $values[0]);
		} else {
			$mapping = $from;
		}
		return strtr($str, $mapping);
	}

	function hook_hotkey_map($hotkeys) {

		$local_map = [];

		foreach ($hotkeys as $k => $v) {

			if (strpos($k, "(") === FALSE) {
				$local_k = $this->mb_strtr($k, "`~!@#$%^&qwertyuiop[]asdfghjkl;'zxcvbnm,./QWERTYUIOP{}ASDFGHJKL:\"|ZXCVBNM<>?",
					"ёЁ!\"№;%:?йцукенгшщзхъфывапролджэячсмитьбю.ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭ/ЯЧСМИТЬБЮ,");

				if ($local_k != $k)
					$local_map[$local_k] = $v;
			}
		}

		return array_merge($hotkeys, $local_map);
	}

	function api_version() {
		return 2;
	}

}
